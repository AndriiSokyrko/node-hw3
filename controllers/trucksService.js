const { Truck } = require('../models/Truck');
const Joi = require('joi');
const {Error} = require('../models/Error.js')
async function addTrucks(req, res) {
  try {
      const _id = new Date().getTime().toString()
      const userId = req.user.userId
      const created_date = new Date().toISOString()
      const body = {
          _id,
          created_by:userId,
          type:req.body.type,
          created_date
      }

      const schema = new Truck(body);

      try {
          schema.validateSync()
          schema.save().then(async (saved) => {
              let mes = await Error.findById({_id: '62ff67749f4b61f1f41a7849'})
              return res.status(200).json({"message": mes.message});
          })

      } catch (err) {
          let mes =  await Error.findById({_id:'62ff718aed13babb9f2714bd'})
          return err.status(400).json({message: mes.message});
      }

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }

}
const getTrucks = async (req, res) => {
  try {
       const trucks = await Truck.find({created_by:req.user.userId})
      if(trucks===null || !trucks.length){
        return res.status(200).json({message:'No trucks'});
      }
      // console.log(trucks);
      const chgTrucks = trucks.map(truck=>{
          return {_id:truck._id,created_by:truck.created_by,assigned_to:truck.assigned_to,
                  type:truck.type, status:truck.status, created_date:truck.created_date}
      })
         return res.status(200).json(
             {
               "trucks":
                   chgTrucks

             }

         );

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
}

const getByIdTrucks = async (req, res) => {
    try {
        const trucks = await Truck.findById({_id:req.params.id,created_by:req.user.userId})
        console.log(trucks);
        if(trucks===null){
            let mes = await  Error.findById({_id:'630039265cd65a4aa38edce7'})
            return res.status(400).json({message: mes.message});
        }
        trucks['logs']= [
            {
                "message": "Load assigned to driver with id "+trucks._id,
                "time":trucks.created_date
            }
        ]
        return res.status(200).json(
            {
                "trucks": [
                    trucks
                ]
            }

        );

    } catch (e) {
        let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
        return res.status(500).json({ message: mes.message});
    }
};

 const deleteByIdTrucks = async (req, res) => {
  try {
      if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
          let mes = await Error.findById({_id: '630046eb5cd65a4aa38edce8'})
          return res.status(400).json({message: mes.message});
      }

      //check if assigned
      const trucks = await Truck.findById({
          _id: req.params.id,
          userId: req.user.userId,
      })
      if (trucks==null) {
          let mes = await Error.findById({_id: '630048945cd65a4aa38edce9'})
          return res.status(400).json({message: mes.message});
      }

      const delTrucks = await Truck.findByIdAndDelete({
          _id: req.params.id,
          userId: req.user.userId,
      })
      if (delTrucks === null) {
          let mes = await Error.findById({_id: '630039265cd65a4aa38edce7'})
          return res.status(400).json({message: mes.message});
      }
      let mes = await Error.findById({_id: '62ff67749f4b61f1f41a784b'})
      return res.status(200).json({"message": mes.message});
  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
};



const updateByIdTrucks = async (req, res, next) => {
  try {
      // check params url
      if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
          let mes = await  Error.findById({_id:'630046eb5cd65a4aa38edce8'})

          return res.status(400).json({ message: mes.message });
      }
      //check body url
    if (!Object.keys(req.body).length) {
        let mes = await  Error.findById({_id:'630046eb5cd65a4aa38edce8'})

        return res.status(400).json({ message: mes.message });
    }

    //check if assigned
      if(trucks.assigned_to){
          let mes = await  Error.findById({_id:'630048945cd65a4aa38edce9'})
          return res.status(400).json({ message: mes.message });
      }

      const _id = new Date().getTime()
      const userId = req.user.userId
      const created_date = new Date().toISOString()
      const body = req.body
      // body['_id']=_id
      body['created_by']=userId
      body['created_date']=created_date
      const schema = new Truck(body);
      schema.validate()
         .then(async ()=>{
              Truck.findByIdAndUpdate({
                  _id:req.params.id,
                     created_by: req.user.userId
                 },
                  {$set:{...schema}} ,{new:false})
              .then(async (load) => {
                  if (!load || typeof load === 'null') {
                      let mes = await  Error.findById({_id:'630046eb5cd65a4aa38edce8'})
                      return res.status(400)
                          .json({ message: mes.message })

                  }
                  let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a784a'})
                  return res.status(200).json({ message: mes.message});
              })
              .catch(async e=>{
              let mes = await  Error.findById({_id:'630046eb5cd65a4aa38edce8'})
              return res.status(400)
                  .json({ message: mes.message })
             })
      }).catch(e=>{
          res.status(400).json({message:"No validate schema"+e})
      })

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
};

const assignTrucks = async (req, res, next) =>  {
    try {
        // check params url
        if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
            let mes = await Error.findById({_id: '630046eb5cd65a4aa38edce8'})

            return res.status(400).json({message: mes.message});
        }
        const updTruck = await Truck.findByIdAndUpdate({
            _id: req.params.id,
            created_by: req.user.userId
        }, {$set: {assigned_to: req.user.userId,status:'OL'}}, {new: true})

        if (updTruck === null) {
            let mes = await Error.findById({_id: '630046eb5cd65a4aa38edce8'})
            return res.status(400).json({message: mes.message})
        }

        let mes = await Error.findById({_id: '62ff67749f4b61f1f41a784c'})
        return res.status(200).json({message: mes.message});

    } catch (e) {
        let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
        return res.status(500).json({ message: mes.message});
    }
}

module.exports = {
    addTrucks,
    getTrucks,
    getByIdTrucks,
    deleteByIdTrucks,
    updateByIdTrucks,
    assignTrucks
};
