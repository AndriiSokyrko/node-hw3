const { Load } = require('../models/Load');
const { Truck } = require('../models/Truck');
const { User } = require('../models/Users');
const Joi = require('joi');
const db = require("../models/Load");
const {Error} = require('../models/Error.js')
async function addLoads(req, res) {
  try {
      const _id = new Date().getTime()
      const assigned_to = req.user.userId
      const userId = req.user.userId
      const created_date = new Date().toISOString()
      const body = req.body
      body['_id']=_id
      body['created_by']=userId
      body['created_date']=created_date
      // body['assigned_to']=assigned_to
      const schema = new Load(body);
          schema.validate().then(()=>{
              schema.save().then(async (saved) => {
                  let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a784d'})
                  return res.status(200).json({"message":mes.message});
              }).catch( async e => {
                  let mes = await  Error.findById({_id:'630046eb5cd65a4aa38edce8'})
                  return res.status(400).json({"message":mes.message});
              });
          }).catch(e=>{
          res.status(400).json({message:"No validate schema"})
      })
  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }

}

const getLoads = async (req, res) => {
  try {
      const user = await User.findById({_id: req.user.userId})
       // let loads = await Load.find({created_by:req.user.userId})

     let  loads = await Load.aggregate([
         {$match: {
          created_by: {
              $eq: req.user.userId
          }
      }}
      ])
      if(loads===null){
          return res.status(200).json('No records');
      }
        if(user.role==='DRIVER'){
            //   loads = await Load.find({"assigned_to":req.user.userId,
            //     "status":{ $in: ["POSTED", "ASSIGNED", "SHIPPED"]}
            // })

              loads = await Load.aggregate([
                {
                    $lookup:
                        {
                            "from": "Truck" ,
                            "localField": "assigned_to"  ,
                            "foreignField": "_id",
                            "as": 'loads'
                        }},
                        {$match: {
                            "assigned_to": req.user.userId,
                            "status":{ $in: ["POSTED", "ASSIGNED", "SHIPPED"]},
                            },
                        },
            ])
        }
      if(loads===null){
        return res.status(200).json('No records');
      }
      let updLoads=[]
      if(loads.length) {
            updLoads = loads.map(load => {

                   const temp = {
                       "logs":
                           [{
                               "message": "Load assigned to driver with id " + load.assigned_to,
                               "time": load.created_date
                           }]
                   }
                return {...load,...temp}
          })
      }
         return res.status(200).json(
             {
               "loads":
                   updLoads

             }

         );

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
}
const activeLoadsPosted = async (req,res)=>{
    try {
        const trucks = await Truck.find({assigned_to:req.user.userId})

        let  loads = await Load.aggregate([
            {$match: {
                    assigned_to: {
                        $eq: req.user.userId
                    },

                }},
            {$match:{
                    status:{ $in: ['POSTED']}
                }

            }
        ])
        console.log(loads);
        if(loads===null){
            let mes = await  Error.findById({_id:'630039265cd65a4aa38edce7'})
            return res.status(500).json({ message: mes.message});
        }

        let updLoads={}
        if(loads.length) {
            loads.forEach(load => {
                delete load['loads']
                delete load['__v']
                const temp = {
                    "logs":
                        {
                            "message": "Load assigned to driver with id " + load.assigned_to,
                            "time": load.created_date
                        }
                }

                const res ={load: {...load,...temp}}
                updLoads = {...updLoads,...res}
            })
        }

        return res.status(200).json(
            {

                ...updLoads

            }

        );

    } catch (e) {
        let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
        return res.status(500).json({ message: mes.message});
    }

}
const activeLoads = async (req, res) => {
  try {
      const trucks = await Truck.find({assigned_to:req.user.userId})

      let  loads = await Load.aggregate([
          {$match: {
                  assigned_to: {
                      $eq: req.user.userId
                  },

              }},
          {$match:{
                  status:{ $in: [ 'ASSIGNED', 'SHIPPED']}
              }

          }
      ])
      console.log(loads);
      if(loads===null){
          let mes = await  Error.findById({_id:'630039265cd65a4aa38edce7'})
          return res.status(500).json({ message: mes.message});
      }

      let updLoads={}
      if(loads.length) {
          loads.forEach(load => {
              delete load['loads']
              delete load['__v']
              const temp = {
                  "logs":
                      {
                          "message": "Load assigned to driver with id " + load.assigned_to,
                          "time": load.created_date
                      }
              }

               const res ={load: {...load,...temp}}
              updLoads = {...updLoads,...res}
          })
      }

      return res.status(200).json(
          {

              ...updLoads

          }

      );

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
}

const getByIdLoads = async (req, res) => {
    try {
        const loads = await Load.findById({_id:req.params.id})
            if(loads===null){
                let mes = await  Error.findById({_id:'630039265cd65a4aa38edce7'})
                return res.status(204).json({message: mes.message});
            }
                let updLoads = {...loads,
                    "logs":
                        [{
                            "message": "Load assigned to driver with id " + loads.assigned_to,
                            "time": loads.created_date
                        }]
                }

        return res.status(200).json(
            {...updLoads}
        );

    } catch (e) {
        let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
        return res.status(500).json({ message: mes.message});
    }
};


const deleteByIdLoads = async (req, res) => {
  try {
    if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
        let mes = await  Error.findById({_id:'630046eb5cd65a4aa38edce8'})

        return res.status(400).json({ message: mes.message });
    }
    const loads = await Load.findById(
        {_id:req.params.id})
      if(loads.status!=='NEW'){
          let mes = await  Error.findById({_id:'630048945cd65a4aa38edce9'})
          return res.status(400).json({ message: mes.message });
      }
     const del = await Load.findByIdAndDelete({
         _id: req.params.id,
         created_by:req.user.userId
     })

      if (del === null) {
          let mes = await  Error.findById({_id:'630039265cd65a4aa38edce7'})

        return res.status(400).json({ message: mes.message });
      }
      let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a7851'})
      return res.status(200).json({"message":mes.message});
  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
};



const updateByIdLoads = async (req, res, next) => {
  try {
      // check params url
      if (typeof req.params.id === 'undefined' || req.params.id.trim === '') {
          let mes = await Error.findById({_id: '630046eb5cd65a4aa38edce8'})

          return res.status(400).json({message: mes.message});
      }
      //check body url
      if (!Object.keys(req.body).length) {
          let mes = await Error.findById({_id: '630046eb5cd65a4aa38edce8'})

          return res.status(400).json({message: mes.message});
      }
      const loads = await Load.findById({_id: req.params.id})
      // if (loads.status !== 'NEW') {
      //     let mes = await Error.findById({_id: '630048945cd65a4aa38edce9'})
      //     return res.status(400).json({message:'Only for status New is  allowed'});
      // }

      const created_date = new Date().toISOString()
      const body = {_id:req.params.id,created_date,...req.body}
      const schema = new Load(body);
      schema.validate()
          .then(async () => {

              const loadsUp = await Load.findByIdAndUpdate({
                  _id: req.params.id,
                  created_by: req.user.userId
              }, {$set: {...schema}}, {new: false})
              if (loadsUp === null) {
                  let mes = await Error.findById({_id: '630046eb5cd65a4aa38edce8'})
                  return res.status(400)
                      .json({message: mes.message})
              }
              let mes = await Error.findById({_id: '62ff67749f4b61f1f41a7850'})
              return res.status(200).json({message: mes.message});
          })
          .catch(async (e) => {
              let mes = await Error.findById({_id: '62ff6f2eed13babb9f2714bb'})
              return res.status(400).json({message: mes.message});
          })

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
};

const searchForDrivers = async (req, res, next) => {
try {
    const driver = await Truck.find({assigned_to:{$ne:null}})
    if(driver===null){
        //driver not found
        let mes = await  Error.findById({_id:'6301ae8ddbbccd5082660824'})
        return res.status(400).json({ message: mes.message});
    }
    const lenArray =  Math.floor(Math.random() * (driver.length)) ;
    // const assignedId= driver[lenArray]._id //id truck
    const assignedId= driver[lenArray].assigned_to //id driver

    const loads = await Load.findOneAndUpdate({_id:req.params.id},{$set:{
        assigned_to:assignedId,status:'ASSIGNED'
        }})

    if(loads===null){
        let mes = await  Error.findById({_id:'62ff6f2eed13babb9f2714bb'})
        return res.status(400).json({ message: mes.message});
    }

    let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a7852'})
    return res.status(200).json({ message: mes.message+' '+assignedId, "driver_found": true});
}catch(e){
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
}
}
const activeStateLoads = async (req, res, next) => {
    try {

        const trucks = await Truck.find({assigned_to:req.user.userId})
        const loads = await Load.aggregate([
            {
                $lookup:
                    {
                        from: "Truck" ,
                        localField: "assigned_to"  ,
                        foreignField: "_id",
                        as: 'loads'
                    }},
            {$match: {
                    "status":{ $in: ['POSTED', 'ASSIGNED', 'SHIPPED']}}},

        ])
        const statusAll =  ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']

        const checkState = loads[0].state
        const statesAll =['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
         let indexCurrent = statesAll.indexOf(checkState)
         let indexNext = indexCurrent<statesAll.length-2? indexCurrent+1:0
        const next =statesAll[indexNext];
        const prev =statesAll[indexCurrent];
        // const setStatus = next==='En route to Pick Up'?'SHIPPED'
        try{

            loads.map( async load=>{
                const id= load._id
                const curStatus = load.status
                const chgStatus = next==='En route to Pick Up'||'En route to delivery'?'SHIPPED': curStatus
                await  Load.findByIdAndUpdate({_id:id},
                    { $set: { state: next,
                        status: chgStatus
                        } }, { new: true })
            })
                let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a7850'})
                return res.status(200).json({ message: `'${mes.message} '${prev} route to ${next}'`})

            }catch(e){
                let mes = await  Error.findById({_id:'630039265cd65a4aa38edce7'})
                return res.status(400).json({ message: mes.message});
            }

  } catch (e) {
    let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
    return res.status(500).json({ message: mes.message});
  }
};

module.exports = {
  addLoads,
  getLoads,
    activeLoads,
    getByIdLoads,
    deleteByIdLoads,
    updateByIdLoads,
    activeStateLoads,
    searchForDrivers,
    activeLoadsPosted
};
