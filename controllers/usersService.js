const bcrypt = require('bcryptjs');
require('dotenv').config()

const { User } = require('../models/Users');
const { Error } = require('../models/Error');
const { Credentials } = require('../models/Credentials');

const changeProfilePassword = async (req, res) => {
    try {
        const schema =   await User.findById({_id:req.user.userId})
        const email = schema.email;

            if(req.body.image){
                await User.findByIdAndUpdate({_id:req.user.userId},
                    {$set:{image:req.body.image}})
            }
        if(req.body.oldPassword.length && req.body.newPassword.length ) {
            const {oldPassword, newPassword} = req.body
            const credentials = await Credentials.findOne({email});

            if (!credentials) {
                let mes = await Error.findById({_id: '62ff6f2eed13babb9f2714bb'})
                return res.status(400).json({message: mes.message});

            }
            bcrypt.compare(String(oldPassword), String(credentials.password),
                async (err, isMatched) => {
                    if (!isMatched) {
                        let mes = await Error.findById({_id: '62ff7123ed13babb9f2714bc'})
                        return res.status(400)
                            .json({message: mes.message});
                    }
                    const cryptPassword = bcrypt.hashSync(newPassword, 10);
                    const credent = await Credentials.findOneAndUpdate(
                        {email: email},
                        {$set: {password: cryptPassword}},
                    )
                    let mes = await Error.findById({_id: '62ff67749f4b61f1f41a7847'})
                    return res.status(200).json({
                        "message": mes.message,
                    })
                })
        }

    } catch (e) {
        let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})

        res.status(500).json({message: mes.message});
    }

};


const deleteProfile = async (req, res) => {
  try {
    return User.findByIdAndDelete({
      _id: req.user.userId,
    })
      .then(async (user) => {
        if (!user) {
            let mes = await  Error.findById({_id:'62ff718aed13babb9f2714bd'})
            return res.status(400).json({ message: mes.message});
        }
          let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a7846'})
          return res.status(200).json({ message: mes.message})
      });

  } catch (e) {
      let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
      return res.status(500).json({ message: mes.message});
  }
};

const getProfileInfo = async (req, res) => {
  try {
    return User.findById({
      _id: req.user.userId,
    })
      .then(async (user) => {

          if (!user || typeof user ==='null') {
              let mes = await  Error.findById({_id:'62ff718aed13babb9f2714bd'})
              return res.status(400).json({ message: mes.message});
        }
        const {_id,
            role,
            email,
            image,
            created_date} = user._doc
        return res.status(200)
          .json({"user":{
              _id,role,email,image,created_date
              }});
      });
  } catch (e) {
      let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
      return res.status(500).json({ message: mes.message});
  }
};
module.exports = {
  changeProfilePassword,
  deleteProfile,
  getProfileInfo,
};
