const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/Users');
const { Credentials } = require('../models/Credentials');
const { Error } = require('../models/Error');
const {user_validate} = require('../validaters/user_validate')
const Joi = require('joi');

require('dotenv').config()
const fillError = async () =>{
   Error.insertMany([
        {"message":"Profile created successfully"},
        {"message":"Profile deleted successfully"},
        {"message":"Password changed successfully"},
        {"message": "New password sent to your email address"},
        {"message":"Truck created successfully"},
        {"message":"Truck details changed successfully"},
        {"message":"Truck deleted successfully"},
        {"message":"Truck assigned successfully"},
        {"message":"Load created successfully"},
        {"message": "Load state changed to 'En route to Delivery'"},
        {"message": "Load assigned to driver with id ###"},
        {"message": "Load details changed successfully"},
        {"message": "Load deleted successfully"},
        {"message": "Load posted successfully"}
    ]).then(() => res.json({
       code: '400000'
   }
   ))
}
const createProfile = async (req, res, next) => {
        try {
            const {email, password ,role} = req.body;
            try {
                 await user_validate.validateAsync({email,password});
            }
            catch (err) {
                let mes = await  Error.findById({_id:'62ff6f2eed13babb9f2714bb'})

                return err.status(400).json({message: mes.message});
            }
            const checkUserName = await User.findOne({email});

            if (checkUserName) {
                let mes = await  Error.findById({_id:'62ff729aed13babb9f2714be'})

                return res.status(400).json({message: mes.message});
            }

            const created_date = new Date().toISOString()
            const cryptPassword = bcrypt.hashSync(password, 10);
            const _id= new Date().getTime()
            const user = new User({
                _id,
                email,
                role,
                created_date
            });
            const credentials = new Credentials({
                email,
                password: cryptPassword,

            });

            user.save()
                .then((saved) => {
                    // return res.status(200).json({message: 'Success', ...saved._doc});
                    credentials.save()
                        .then(async (saved) => {
                            let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a7845'})
                            return res.status(200).json({message: mes.message})
                        })

                        .catch((err) => {
                            next(err);
                        });

                })
                .catch((err) => {
                    next(err);
                });

        } catch (e) {
            let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})
            res.status(500).json({message: mes.message});
        }
};

function genPassword(){
    const pwdChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    const pwdLen = 6;
    return Array(pwdLen).fill(pwdChars).map(function(x) {
        return x[Math.floor(Math.random() * x.length)]
    }).join('');
}

const forgot = async (req, res, next) => {
    try {
        const {email} = req.body;
        try {
            await user_validate.validateAsync({email});
        } catch (err) {
            let mes =  await Error.findById({_id:'62ff718aed13babb9f2714bd'})

            return err.status(400).json({message: mes.message});
        }
        const user = await Credentials.findOne({ email});
        if(user){
            const newPassword = genPassword()
            const cryptPassword = bcrypt.hashSync(newPassword, 10);
            const credent =  await Credentials.findOneAndUpdate(
                { email:email },
                { $set: { password: cryptPassword } },
            )
            if (!credent) {
                let mes = await  Error.findById({_id:'62ff6f2eed13babb9f2714bb'})

                return res.status(400)
                    .json({ message: mes.message });
            }
            let mes = await  Error.findById({_id:'62ff67749f4b61f1f41a7848'})
            return res.status(200).json({
                "message": mes.message,
                newPassword
            })

        } else{
            let mes = await  Error.findById({_id:'62ff6f2eed13babb9f2714bb'})

        }
        let mes = await  Error.findById({_id:'62ff6f2eed13babb9f2714bb'})
            return res.status(400).json({message: mes.message});


    } catch (e) {
        let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})

        res.status(500).json({message: mes.message});
    }
}

const login = async (req, res, next) => {
  try {
      const {email, password } = req.body;
      try {
          await user_validate.validateAsync({email,password});
      }
      catch (err) {
          let mes = await  Error.findById({_id:'62ff6f2eed13babb9f2714bb'})
          return err.status(400).json({message: mes.message});
      }
    const user = await User.findOne({ email});
      if(user===null){
          return res.status(400).json({message: 'User not found'});
      }
    const credentials = await Credentials.findOne({ email});
    if(user){
    bcrypt.compare(String(password), String(credentials.password),
        async (err, isMatched)=>{
            if(!isMatched){
                let mes = await  Error.findById({_id:'62ff7123ed13babb9f2714bc'})
                return res.status(400)
                    .json({message:mes.message});
            }
          const payload = {
              userId: user._id,
              username: user.email,
          };

          const jwtToken =jwt.sign(payload,  process.env.SECRET_KEY);
          return res.status(200).json({
            jwt_token: jwtToken,
          })
        })

    } else {
        let mes = await  Error.findById({_id:'62ff718aed13babb9f2714bd'})
      return res.status(400)
          .json({message: mess.message});
    }
  } catch (e) {
      let mes = await  Error.findById({_id:'62ff6e5bed13babb9f2714ba'})

      res.status(500).json({ message: mes.message });
  }
};

module.exports = {
  createProfile,
  login,
  forgot,
  fillError
};
