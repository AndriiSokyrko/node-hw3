require('dotenv').config()
const jwt = require('jsonwebtoken');
const authMiddleware = (req, res, next) => {

  const {
    authorization,
  } = req.headers;
  if (!authorization) {
    return res.status(401).json({ message: 'Please, provide authorization header' });
  }

  let [,token] = authorization.split(' ');
  if (!token) {
    return res.status(401).json({ message: 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email
    };
    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
