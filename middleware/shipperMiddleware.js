require('dotenv').config()
const jwt = require('jsonwebtoken');
const {User} = require('../models/Users')

const shipperMiddleware = async (req, res, next) => {
  try{
  const user = await User.findById({_id:req.user.userId})
  const role = user.role
  if (role!=='SHIPPER') {
    return res.status(401).json({ message: 'Only role SHIPPER'});
  }

    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = {
  shipperMiddleware
};
