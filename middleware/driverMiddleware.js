require('dotenv').config()
const jwt = require('jsonwebtoken');
const {User} = require('../models/Users')

const driverMiddleware = async (req, res, next) => {
  try{
  const user = await User.findById({_id:req.user.userId})
  const role = user.role

    if (role!=='DRIVER') {
    return res.status(401).json({ message: 'Only role DRIVER'});
  }

    next();
  } catch (err) {
    return res.status(401).json({ message: err.message });
  }
};

module.exports = {
    driverMiddleware,
};
