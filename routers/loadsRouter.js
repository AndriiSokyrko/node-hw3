const express = require('express');

const loadsRouter = express.Router();

const {
  addLoads,
  getLoads,
  activeLoads,
  getByIdLoads,
  deleteByIdLoads,
  updateByIdLoads,
  activeStateLoads,
  searchForDrivers,
  activeLoadsPosted
} = require('../controllers/loadsService.js');

const { authMiddleware } = require('../middleware/authMiddleware.js');
const { driverMiddleware } = require('../middleware/driverMiddleware.js');
const { shipperMiddleware } = require('../middleware/shipperMiddleware.js');

loadsRouter.post('/', authMiddleware,shipperMiddleware, addLoads);
loadsRouter.post('/:id/post', authMiddleware,shipperMiddleware, searchForDrivers);

loadsRouter.get('/', authMiddleware, getLoads);

loadsRouter.get('/active', authMiddleware,driverMiddleware, activeLoads);
loadsRouter.get('/active/state/posted', authMiddleware,driverMiddleware, activeLoadsPosted);
loadsRouter.get('/:id', authMiddleware, getByIdLoads);
loadsRouter.put('/:id', authMiddleware, shipperMiddleware, updateByIdLoads);
loadsRouter.patch('/active/state', authMiddleware, driverMiddleware,activeStateLoads);
loadsRouter.delete('/:id', authMiddleware, shipperMiddleware, deleteByIdLoads);

module.exports = {
   loadsRouter
};
