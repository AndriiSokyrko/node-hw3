const express = require('express');

const trucksRouter = express.Router();

const {
    addTrucks,
    getTrucks,
    getByIdTrucks,
    deleteByIdTrucks,
    updateByIdTrucks,
    assignTrucks
} = require('../controllers/trucksService.js');

const { authMiddleware } = require('../middleware/authMiddleware.js');

const { driverMiddleware } = require('../middleware/driverMiddleware.js');
trucksRouter.post('/', authMiddleware,driverMiddleware, addTrucks);

// trucksRouter.get('/', authMiddleware, driverMiddleware,getTrucks);
trucksRouter.get('/', authMiddleware, driverMiddleware,getTrucks);

trucksRouter.get('/:id', authMiddleware, driverMiddleware,getByIdTrucks);

trucksRouter.put('/:id', authMiddleware, driverMiddleware,updateByIdTrucks);

trucksRouter.delete('/:id', authMiddleware, driverMiddleware,deleteByIdTrucks);

trucksRouter.post('/:id/assign', authMiddleware,driverMiddleware, assignTrucks);

module.exports = {
   trucksRouter
};
