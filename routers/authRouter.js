const express = require('express');

const authRouter = express.Router();
const {
  createProfile,
  login,
  forgot,
  fillError
} = require('../controllers/authService.js');

authRouter.post('/register', createProfile);

authRouter.post('/login', login);

authRouter.post('/forgot_password', forgot);

authRouter.post('/error', fillError );


module.exports = {
  authRouter
};
// export default authRouter
