const mongoose = require('mongoose');

const Credentials = mongoose.model('credentials', {
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },

});


module.exports = {
  Credentials,
};
