const mongoose = require('mongoose');

const Truck = mongoose.model('truck', {
  _id:{
    type: Number,
    required: false,
  },
  created_by:{
    type: String,
    required: true,
  },
  assigned_to:{
    type: String,
    required: true,
    default:' '
  },
  type:{
    type: String,
    enum : ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    default: 'SPRINTER'
  },
  status:{
    type: String,
    enum:['OL', 'IS'],
    default:'IS'
  },
  created_date:{
    type: String,
    required: true,
  }

});

module.exports = {
  Truck,
};
