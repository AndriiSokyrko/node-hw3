const mongoose = require('mongoose');

const RegistrationCredentials = mongoose.model('registrationCredentials', {
    email:{
        type: String,
        required: true,
    },
    password:{
        type: String,
        required: true,
    },
    role:{
        type: String,
        enum : ['SHIPPER', 'DRIVER'],
        default: 'SHIPPER'
    },


});

module.exports = {
    RegistrationCredentials,
};
