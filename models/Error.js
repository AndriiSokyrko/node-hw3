const mongoose = require('mongoose');

const Error = mongoose.model('error', {

    message: {
        type: String,
        required: false,
        unique:true
    },

});

module.exports = {
    Error,
};
