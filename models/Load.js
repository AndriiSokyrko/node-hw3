const mongoose = require('mongoose');

const Load = mongoose.model('load', {
  _id:{
    type: String,
    required:false

  },
  created_by:{
    type: String,
    required:true

  },
  assigned_to: {
    type: String,
    required:true,
    default:' '
  },

  status:{
    type: String,
    enum:['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW'

  },
  state:{
    type: String,
    enum : ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
    default: 'En route to Pick Up'

  },
  name:{
    type: String,
    required:true

  },
  payload:{
    type: Number,
    required:true

  },
  pickup_address:{
    type: String,
    required:true

  },
  delivery_address:{
    type: String,
    required:true

  },
  dimensions: {
    width: {
      type: Number,
      required:true,
      default: 0

    },
    length: {
      type: Number,
      required:true,
      default: 0
    }
   ,
    height:{
      type: Number,
      required:true,
      default: 0
    }
  },
  created_date:{
    type: String,
    required:true
  }

});

module.exports = {
  Load,
};
