const mongoose = require('mongoose');

const User = mongoose.model('user', {
  _id: {
    type: String,
    required: false,
  },
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: false,
  },
  created_date: {
    type: String,
    required: true
  }
});

module.exports = {
  User,
};
