const Joi = require('joi');

const loads_validate = Joi.object({
    name: Joi.string().min(3),
    payload: Joi.number(),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    width: Joi.number(),
    length: Joi.number(),
    height: Joi.number(),
    created_date: Joi.string()
})
module.exports={
    loads_validate
}
