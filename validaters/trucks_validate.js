const Joi = require('joi');

const trucks_validate = Joi.object({
    _id: Joi.number(),
    created_by: Joi.string().required(),
    assigned_to: Joi.string().required(),
    type: Joi.string().required(),
    status: Joi.string().required(),
    created_date: Joi.string().required()
})
module.exports={
    trucks_validate
}
