const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const fs = require("fs");

const {loadsRouter} = require("./routers/loadsRouter.js");
const {trucksRouter} = require("./routers/trucksRouter.js");
const {usersRouter} = require("./routers/usersRouter.js");
const {authRouter} = require("./routers/authRouter.js");

const port = process.env.PORT || 8080;
const app = express();
app.use(morgan('common', {
  stream: fs.createWriteStream('./access.log', {flags: 'a'})
}));
dotenv.config();

mongoose.connect(process.env.URL_MONGO).then(() => {
  console.log("Mongo is connected!");
});


app.use(express.json());
app.use(morgan("tiny"));
app.use(cors());

// app.use(express.urlencoded()); // middleware

app.use("/api/users/me", usersRouter);
app.use("/api/loads", loadsRouter);
app.use("/api/trucks", trucksRouter);
app.use("/api/auth", authRouter);


const start = async () => {
  try {

    app.listen(port, (err) => {
      console.log("Server has been started!");
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
app.use(errorHandler);
function errorHandler(err, req, res, next) {
  const status = err.status || 500;
  const message = err.message || "String";
  res.status(status).json({ message });
}

