import './App.scss';
import { useState} from "react";
import {BrowserRouter} from 'react-router-dom'
import AppRouters from "./components/AppRouters";
import {AuthContext} from "./components/context";
function App() {

        const  [userInfo, setUserInfo]= useState({email:'',role:''})

  return (
      <AuthContext.Provider value= {{userInfo, setUserInfo}}>
          <BrowserRouter >
              <div className = "main" >
                  <AppRouters />
              </div >
          </BrowserRouter >
      </AuthContext.Provider >
  );
}

export default App;
