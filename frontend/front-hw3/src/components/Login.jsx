import React, { useEffect,  useState} from 'react';
import   './Auth.scss'
import {Link} from "react-router-dom";
import {setQuery} from "../helper/helper";
import {useNavigate} from "react-router";
const Login = (props) => {
    const navigate = useNavigate();

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')

     const login = async ()=>{
         if(!password || !email){
             return
         }
        const res = await setQuery('/api/auth/login', 'post', {email,password})
         if(res.status===200){
            localStorage.setItem("jwt_token",'JWT ' +res.data.jwt_token)
             const info = await setQuery('/api/users/me', 'get')
             // console.log(info.data.user);
             localStorage.setItem('email',info.data.user.email)
             localStorage.setItem('role',info.data.user.role)
             navigate('/user')
         }else{
             setMessage(res.message)
             setTimeout(()=>{
                 setMessage('')
             },1000)
         }
     }
     useEffect(()=>{
         if(window.localStorage.jwt_token){
             navigate('/user')
         }
     },[navigate])

    return (
        <div className="auth">
            <div>{message}</div>
            <div className="wrapper--title" >
                <a href = "/" className = "logo" >LOGO</a >
                <h1 className = "title" >Form authenfication</h1 ></div >
            <label htmlFor = "email" >
            <input className="email" type = "email" id="email" placeholder="Email" value={email} required ={true}
                   onChange={e=>setEmail(e.target.value)}/>
            </label >
            <label htmlFor = "password" >
            <input className="password" type = "password" id="password" placeholder="Password" value={password} required ={true}
                   onChange={e=>setPassword(e.target.value)}/>
            </label >
            <input className="login" type = "button" value="Login" onClick={login}/>

            <div className="wrapper--link">
                <Link to="/register" className="register" >Registration</Link>
                <Link to="/forgot" className="forgot">Forgot password</Link>
            </div>


        </div >
    );
};

export default Login;
