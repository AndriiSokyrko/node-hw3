import React from 'react';
import UserFormHeader from "./User-form-header";
import UserFormSidebar from "./User-form-sidebar";
import UserFormMain from "./User-form-main";
import './User-form.scss'
import {useNavigate} from "react-router-dom";
const UserForm = () => {
    const navigate = useNavigate()
    if(localStorage.getItem('jwt_token')===''){
        navigate('/')
        return
    }
    return (
        <div className="wrapper--form">
            <UserFormHeader/>
            <div className="wrapper--body">
                <UserFormSidebar/>
                <UserFormMain/>

            </div>

        </div >
    );
};

export default UserForm;
