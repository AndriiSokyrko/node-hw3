import React, {useState} from 'react';
import   './Register.scss'
import {Link, useNavigate} from "react-router-dom";
import {setQuery} from "../helper/helper";
const Register = () => {
    const navidate = useNavigate()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('SHIPPER')
    const [status, setStatus] = useState('')
    const register = async ()=>{
        if(!password || !email){
            return
        }
        const res = await setQuery('/api/auth/register', 'post', {email,password,role})
        if(res.status===200){
            setStatus('You\'re succesfull redistered')
        } else{
            setStatus('You\'re not redistered')
        }
        setTimeout(()=> {
            setStatus('')
            navidate('/login')
        },1000)

    }
    return (
        <div className="auth">
            <div id="status_register">{status}</div>
            <div className="wrapper--title" >
                <a href = "/" className = "logo" >LOGO</a >
                <h1 className = "title" >Form registration</h1 ></div >
            <label htmlFor = "email" >
                <input className="email" type = "email" id="email" placeholder="Email" value={email}
                 onChange={e=>setEmail(e.target.value)}/>
            </label >
            <label htmlFor = "password" >
                <input className="password" type = "password" id="password" placeholder="Password" value={password}
                 onChange={e=>setPassword(e.target.value.trim())}/>
            </label >

            <label htmlFor = "role" onChange={e=>setRole(e.target.value)}>
                <select name = "role" id = "role" className="role">
                    <option value = "SHIPPER" >SHIPPER</option >
                    <option value = "DRIVER" >DRIVER</option >
                </select >

            </label >

            <input className="login" type = "button" value="register" onClick={register}/>

            <div className="wrapper--link">
                <Link to="/login" className="login">Login</Link>
            </div>


        </div >
    );
};

export default Register;
