import React from 'react';
import './User-form-main.scss'
import {Outlet} from "react-router-dom";

const UserFormMain = () => {
    return (
        <div className="wrapper--user_main">

                <Outlet/>
        </div >
    );
};

export default UserFormMain;
