import React, {useEffect, useState} from 'react';
import './Card-loads.scss'
import {setQuery} from "../../../helper/helper";

const CardLoads = (props) => {
    const[message, setMessage] = useState()
    const [loads, setLoads] =useState([])
    const [check, setCheck] =useState(true)
    const changeLoad =  async ()=>{
        try {
            const id = props.elm._id
            const res =  await setQuery('/api/loads/'+id,'put',{...loads})
            if(res.status!==200) {
                throw  Error("Only status New");
            }
            props.changeCheck()

        } catch(e){
            setMessage(e.message)
            setTimeout(()=>{
                setMessage('')
            },1000)

        }
    }

const deletePosted = async (e)=>{
    const id=  e.target.attributes.datadelete.value
     await setQuery('/api/loads/'+id, 'delete')
    props.changeCount(props.count-1)
    props.changeCheck()

}
    const setPosted = async (e)=>{
        const id=  e.target.attributes.dataposted.value

        const res = await setQuery('/api/loads/'+id+'/post', 'post',{id:id})
        if(res.status!==200 || res.message=== "Trucks not found"){
            setMessage('Trucks not found')
            setTimeout(()=>{
                setMessage('')
            },1000)

        }
        props.changeCount(props.count)
        props.changeCheck()

    }
    useEffect(()=> {
        setLoads(({...props.elm}))
        setCheck(false)
    },[check,props.elm])

        return (
            <div className = "wrapper--cards_unit" >
                <div>{message}</div>
                <label htmlFor = "text" >_id
                    <input className = "text" type = "text" id = "text" value={loads._id||''} readOnly= {true}/>
                </label >
                <label htmlFor = "email" >created_by
                    <input className = "text" type = "email" id = "email"
                           value={props.elm.created_by} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >delivery_address
                    <input className = "text" type = "text" id = "text" value={loads.delivery_address ||''}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >name
                    <input className = "text" type = "text" id = "text" value={loads.name ||''}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >payload
                    <input className = "text" type = "text" id = "text" value={loads.payload || 0}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >pickup_address
                    <input className = "text" type = "text" id = "text" value={loads.pickup_address ||''}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >state
                    <input className = "text" type = "text" id = "text" value={loads.state ||''}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >status
                    <input className = "text" type = "text" id = "text" value={loads.status ||''}
                           readOnly= {true}/>
                </label >

            </div >
    );
};

export default CardLoads;
