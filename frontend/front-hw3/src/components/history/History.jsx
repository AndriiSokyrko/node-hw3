import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import {setQuery} from "../../helper/helper";
import CardLoads from "../new-loads/Card-loads";

const History = () =>  {
// console.log(props);
    const setPosted = async ()=>{
        const id= props.elm.refLoad._id
        const res = await setQuery('/api/loads/'+id+'/post', 'post',{_id:id})
        return res
    }
    return (
        <div className = "auth" >
            <label htmlFor = "email" >
                <input className = "email" type = "email" id = "email"
                       value={props.elm.created_by} readOnly= {true}/>
            </label >
            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm._id} readOnly= {true}/>
            </label >

            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm.delivery_address} readOnly= {true}/>
            </label >
            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm.name} readOnly= {true}/>
            </label >
            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm.payload} readOnly= {true}/>
            </label >
            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm.pickup_address} readOnly= {true}/>
            </label >
            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm.state} readOnly= {true}/>
            </label >
            <label htmlFor = "text" >
                <input className = "text" type = "text" id = "text" value={props.elm.status} readOnly= {true}/>
            </label >

            <input className="login" type = "button" value="Set posted" onClick={setPosted}/>

        </div >
    );
}

export default History;
