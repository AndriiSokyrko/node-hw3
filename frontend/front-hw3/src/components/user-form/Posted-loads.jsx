import React, {useEffect, useState} from 'react';
import {setQuery} from "../../helper/helper";
import CardPostedLoads from "../posted-loads/Card-posted-loads";
import {useNavigate} from "react-router-dom";

const PostedLoads = () => {
        const [loads, setLoads] =useState([])
        const [check, setCheck] =useState(true)
        const [message, setMessage] =useState('')
        const navigate = useNavigate()
        const fetchDate = async () =>{
            const res = await setQuery('/api/loads/active/state/posted')
            return res
        }
        const changeCheck =()=>{
            setCheck(!check)
        }
        useEffect(()=>{

            new Promise((resolve => {
                const res = fetchDate();
                if(res.status!==200){
                    setMessage('Not found loads')
                }
                resolve(res)
            })).then(data=>{
                if(!Object.keys(data).length){
                    setMessage('No data')
                    navigate('/user')
                    setTimeout(()=>{
                        setMessage('')

                    },1000)

                }
                setLoads(data.data)
            })
        },[check])

        return (
            <div >
                <div>{message}</div>
                {

                    // loads.map((elm) => {
                    Object.keys(loads).map(key=>{
                        let elm= loads[key]
                        return <CardPostedLoads changeCheck={changeCheck} elm = {{...elm}} key={elm._id}/>

                    })
                    // })
                }
            </div >
        );
};

export default PostedLoads;
