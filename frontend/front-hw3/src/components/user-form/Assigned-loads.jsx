import React, { useEffect, useState} from 'react';
import './Assigned-loads.scss'
import {setQuery} from "../../helper/helper";
import CardLoads from "../new-loads/Card-loads";
import {useNavigate} from "react-router-dom";
const AssignedLoads = () => {
    const [loads, setLoads] =useState([])
    const [check, setCheck] =useState(true)
    const [message, setMessage] =useState('')

    const navigate = useNavigate()

    const fetchDate = async () =>{
        const res = await setQuery('/api/loads/active', 'get')
        return res
    }

    useEffect(()=>{
        new Promise(((resolve) => {
            const res = fetchDate();
            resolve(res)

        })).then(data=>{
            if(!Object.keys(data).length){
                setMessage('No data')
                navigate('/user')
                setTimeout(()=>{
                    setMessage('')

                },1000)

            }
            setLoads(data.data)
            setCheck(false)
        })

    },[check])

    return (
        <div >
                <div>{message}</div>
            {
                    Object.keys(loads).map(key=>{
                    let elm= loads[key]
                    return  <CardLoads elm={{...elm}} key={elm._id}/>

                })
            }
        </div >
    );
}

export default AssignedLoads;
