import './New-loads.scss'
import React, {useCallback,  useEffect,  useState} from 'react';
import { useNavigate} from "react-router-dom";
import {setQuery} from "../../helper/helper";
import CardLoads from "../new-loads/Card-loads";
import UidButton from "../uid/uid-button";
import {store} from "@emailjs/browser/es/store/store";
const NewLoads = () => {
    const navigate = useNavigate()
    const [loads, setLoads] =useState([])
    const [check, setCheck] =useState(true)
    const [arrayPagin, setArrayPagin] = useState([])
    const [count, setCount] = useState(0)
    const [activeBtn, setActiveBtn] = useState(0)

  const countCard = useCallback((count)=> {
      const total = []
      for (let i = 1; i <= count; i++) {
          total[i]=i
      }
      setArrayPagin(total)
  },[])

    const changeCheck =()=>{
        setCheck(!check)
    }
    const changeCount =(num)=>{
        setCount(num)
    }
    const next=(p)=>{
        setCount(p-1)
        setActiveBtn(p)

    }

    const addNew =()=>{
        navigate("/user/addLoad", { replace: true });
    }
    const fetchDate = async () =>{
            const res = await setQuery('/api/loads', 'get')
           return res
       }

    useEffect(()=> {
        fetchDate().then(data => {
            if(!data.length){
                return
            }
            setLoads(data.data.loads)
            countCard(data.data.loads.length)
        })
        setCheck(false)
    },[check])

    return (
        <div className="wrapper--cards_header">
            {localStorage.getItem("role") !== 'DRIVER' ?
                <div >
                    <input type = "button" value = "New load" onClick = {addNew} />
                </div > :
                ''
            }
            <div className = "wrapper--cards" >
                {
                    loads.map((elm,index) => {
                            return index===count?<CardLoads changeCheck = {changeCheck}
                              count={count} changeCount={changeCount} elm = {elm}
                                                            key = {elm._id} />: ''
                        })

                }
            </div >
            <div className="wrapper--pagination">
             {
                 arrayPagin.map(p => {
                         return <UidButton activeBtn={activeBtn} changeCheck = {changeCheck} next = {next} name = {p} key = {p} />
                     })
             }
            </div>
        </div>
    );
};

export default NewLoads;
