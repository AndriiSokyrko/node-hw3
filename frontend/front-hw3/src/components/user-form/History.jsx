import React, {useEffect, useState} from 'react';
import './History.scss'
import {setQuery} from "../../helper/helper";
import CardLoads from "../history/history-loads/Card-loads";
const History = () => {
    const [loads, setLoads] =useState([])
    const [check, setCheck] =useState(true)

    const fetchDate = async () =>{
        const res = await setQuery('/api/loads', 'get')
        return res
    }

    useEffect(()=>{
        new Promise((resolve => {
            const res = fetchDate();
            resolve(res)
        })).then(data=>{
            console.log(data);
            setLoads(data.data.loads)
            setCheck(false)
        })


    },[check])

    return (
        <div >

            {
                loads.map((elm) => {
                    console.log(elm);
                    return  <CardLoads elm={{...elm}} key={elm._id}/>

                })
            }
        </div >
    );
}

export default History;
