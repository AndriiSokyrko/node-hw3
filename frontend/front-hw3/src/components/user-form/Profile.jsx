import React, {useEffect, useState} from 'react';
import './Profile.scss'
import {setQuery} from "../../helper/helper";
import CardProfile from "../profile/Card-profile";
const Profile = () => {
    const [profile, setProfile] =useState(null)
    const [check, setCheck] =useState(true)

    const fetchDate =  async () =>{
        return  await setQuery('/api/users/me')
    }
    useEffect(()=>{
        const data =   fetchDate()
        data.then(res=>{
            setProfile(res.data.user)
            setCheck(false)
        }).catch(e=>{
        })
    },[check])

    return (
            <div className="wrapper--profile">
                {(()=>{
                    if(!check){
                      return  <CardProfile profile={profile}/>
                }
                })()}


            </div >

    );
};

export default Profile;
