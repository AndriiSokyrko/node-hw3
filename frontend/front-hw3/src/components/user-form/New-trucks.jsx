import React, {  useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import {setQuery} from "../../helper/helper";
import CardTrucks from "../new-trucks/Card-trucks";
const NewTrucks = () => {
    const [trucks, setTrucks] =useState([])
    const [check, setCheck] =useState(true)
    const [message, setMessage] =useState('')

    const navigate = useNavigate()
const addNew =()=>{
    navigate("/user/addTruck", { replace: true });
}
    const fetchDate = async () =>{
            return await setQuery('/api/trucks', 'get')
       }

       const changeCheck= ()=>{
        setCheck(!check)
     }

    useEffect(()=>{
        setMessage('')

        new Promise(((resolve, reject) => {
            const res = fetchDate();
            resolve(res)
            reject(res)
        })).then(data=>{
            if(!data.length){
                setMessage('No trucks')
                return
            }
            // console.log(data);
            setTrucks(data.data.trucks)
            setCheck(false)
        })
    },[check])
    return (
        <div className="wrapper--trucks">
            <div>{message}</div>
            { localStorage.getItem("role")==='DRIVER'?
                <div >
                    <input type = "button" value = "New Truck" onClick = {addNew} />
                </div >:
                ''
            }
            {  !message.length?
                trucks.map((elm) => {
                      return  <CardTrucks setCheck={changeCheck} elm={{elm}} key={elm._id}/>

                }):
                ''
            }
        </div >
    );
};

export default NewTrucks;
