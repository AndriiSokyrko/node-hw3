import React, {useCallback, useEffect, useState} from 'react';
import './uid-button.scss'

const UidButton = (props) => {
    const [classStr, setClassStr] = useState('uid--button')
    const next = () => {
        props.next(props.name)
        setClassStr("uid--button active")
        props.changeCheck()
    }
    useEffect(() => {
        if(props.activeBtn === props.name) {
            setClassStr("uid--button active")
        } else {
            setClassStr("uid--button")
        }
    }, [props.changeCheck])
    return (
        <div >
            <button className = {classStr} onClick = {next} >{props.name}</button >
        </div >
    );
};

export default UidButton;
