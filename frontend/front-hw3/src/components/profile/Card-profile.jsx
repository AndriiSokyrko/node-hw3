import React, {useCallback, useState} from 'react';
import './Card-profile.scss'
import {setQuery} from "../../helper/helper";
import {useNavigate} from "react-router-dom";
const CardProfile = (props) => {
    const [message, setMessage] =useState()
    const navigate = useNavigate()
    const profile= props.profile
    const [edit, setEdit] = useState({ _id:profile._id,image:profile.image,email:profile.email,
      role:profile.role,  newPassword:'', oldPassword:''});

    const deleteProfile=async ()=>{
        await setQuery('/api/users/me','delete')
        navigate('/auth/register')
    }
    const handleChangeAvatar =  useCallback((e) =>{
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
            setEdit({...edit, image: reader.result});
        }
    },[edit])

    const handleChangeNewPassword = useCallback((e) =>{
        const value = e.target.value
        setEdit({...edit, newPassword:value})
    },[edit])
    const handleChangeOldPassword = useCallback((e) =>{
        const value = e.target.value
        setEdit({...edit, oldPassword:value})
    },[edit])

    const saveProfile =  async (e) =>{
        let body = {
            image: edit.image,
            newPassword: edit.newPassword,
            oldPassword: edit.oldPassword,
        }
         await setQuery('/api/users/me','patch',body)
        setMessage('Profile is changed')
        setTimeout(()=>{
            setMessage('')
        },1000)
    }

    return (
        <div className="wrapper--cards_unit">
        <div>{message}</div>
        <div className="card-wrapper">

            <div className="wrapper-img">
                <img className = "text-info span-start img-responsive"
                     src = {edit? edit.image:"https://i.stack.imgur.com/lAwxR.png"} alt = "avatar" />

                <div className = "form-group" >
                    <input id = "avatar" className = "form-control" type = "file"
                           onChange = {handleChangeAvatar} />
                </div>
            </div>
            <div className="main-context" >
                <label htmlFor = "_id" >
                <input className = "password" type = "text" id = "id"
                       value = {profile._id} readOnly = {true} />
                </label >

                <label htmlFor = "email" >
                    <input className = "password" type = "text" id = "email"
                           value = {edit.email} readOnly={true} />
                </label >
                <label htmlFor = "role" >
                    <input className = "role" type = "text" id = "role"
                           value = {edit.role} readOnly={true}/>
                </label >
                {/*<label htmlFor = "role" onChange={handleChangeRole}>*/}
                {/*    <select name = "role" id = "role" className="role">*/}
                {/*        <option value = "SHIPPER" >SHIPPER</option >*/}
                {/*        <option value = "DRIVER" >DRIVER</option >*/}
                {/*    </select >*/}

                {/*</label >*/}

                <label htmlFor = "created_date" >
                    <input className = "password" type = "text" id = "created_date"
                           value = {profile.created_date} readOnly = {true} />
                </label >
                <label htmlFor = "" ><h6>Change password</h6></label >
                <label htmlFor = "oldPassword" >
                    <input className = "password" type = "password" id = "oldPassword"
                           placeholder="Old password"
                           value = {edit.oldPassword} onChange={handleChangeOldPassword}/>
                </label >
                <label htmlFor = "newPassword" >
                    <input className = "password" type = "password" id = "newPassword"
                           placeholder="Old password"
                           value = {edit.newPassword} onChange={handleChangeNewPassword}/>
                </label >
                <input className="login" type = "button" value="Save" onClick={saveProfile}/>
                <input className="login" type = "button" value="Delete" onClick={deleteProfile}/>

            </div >

        </div >
        </div>
    );
};

export default CardProfile;
