import React, {useEffect, useState} from 'react';
import './Card-loads.scss'
import {setQuery} from "../../helper/helper";

const CardLoads = (props) => {
    const[message, setMessage] = useState()
    const [loads, setLoads] =useState([])
    const [check, setCheck] =useState(true)
    const changeLoad =  async ()=>{
        try {
            const id = props.elm._id
            const res =  await setQuery('/api/loads/'+id,'put',{...loads})
            if(res.status!==200) {
                throw  Error("Only status New");
            }
            props.changeCheck()

        } catch(e){
            setMessage(e.message)
            setTimeout(()=>{
                setMessage('')
            },1000)

        }
    }

const deletePosted = async (e)=>{
        try {
            const id=  e.target.attributes.datadelete.value
            const res =await setQuery('/api/loads/'+id, 'delete')
            if (res.status!==200) {
                throw Error('Only status new')
            }
            props.changeCount(props.count-1)
            props.changeCheck()

        } catch(e){
            setMessage(e.message)
            setTimeout(() => {
                setMessage('')
            }, 1000)

        }


}
    const setPosted = async (e)=>{
        try {
            const id = e.target.attributes.dataposted.value
            const res = await setQuery('/api/loads/' + id + '/post', 'post', {id: id})
            if (res.status!==200) {
                    throw Error('No turcks found')
            }

            setMessage(res.data.message)
            setTimeout(() => {
                setMessage('')
            }, 1000)
            props.changeCheck()
        } catch (e){
                setMessage(e.message)
                setTimeout(() => {
                    setMessage('')
                }, 1000)
        }
    }
    useEffect(()=> {
        setLoads(({...props.elm}))
        setCheck(false)
    },[check,props.elm])
        return (
            <div className = "wrapper--cards_unit" >
                <div>{message}</div>
                <label htmlFor = "text" >_id
                    <input className = "text" type = "text" id = "_id" value={loads._id ||''} readOnly= {true}/>
                </label >
                <label htmlFor = "email" >created_by
                    <input className = "text" type = "email" id = "email"
                           value={props.elm.created_by ||''} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >delivery_address
                    <input className = "text" type = "text" id = "delivery_address" value={loads.delivery_address||''}
                           onChange = {(e) => setLoads({...loads, delivery_address: e.target.value})} />
                </label >
                <label htmlFor = "text" >name
                    <input className = "text" type = "text" id = "name" value={loads.name ||''}
                           onChange = {(e) => setLoads({...loads, name: e.target.value})} />
                </label >
                <label htmlFor = "text" >payload
                    <input className = "text" type = "number" id = "payload" value={loads.payload || 0}
                           onChange = {(e) => setLoads({...loads, payload: e.target.value})} />
                </label >
                <label htmlFor = "text" >pickup_address
                    <input className = "text" type = "text" id = "pickup_address" value={loads.pickup_address ||''}
                           onChange = {(e) => setLoads({...loads, pickup_address: e.target.value})} />
                </label >
                <label htmlFor = "text" >state
                    <input className = "text" type = "text" id = "state" value={loads.state||''}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >status
                    <input className = "text" type = "text" id = "status" value={loads.status||''}
                           readOnly= {true}/>
                </label >
                <label htmlFor = "text" >assigned_to
                    <input className = "text" type = "text" id = "assigned_to" value={loads.assigned_to||''}
                           readOnly= {true}/>
                </label >
                { localStorage.getItem('role')==='SHIPPER'?
                    <div className = "wrapper--btn " >
                        <input className = "login" type = "button" dataposted = {loads._id} value = "Set posted"
                               onClick = {setPosted} />
                        <input className = "login" type = "button" datadelete = {loads._id} value = "Delete"
                               onClick = {deletePosted} />
                        <input className = "login" type = "button" value = "Change Load" onClick = {changeLoad} />

                    </div >: ''
                }
            </div >
    );
};

export default CardLoads;
