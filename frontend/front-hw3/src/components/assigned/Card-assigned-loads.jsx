import React from 'react';
import './Card-assigned-loads.scss'
import {setQuery} from "../../helper/helper";
const CardAssignedLoads = (props) => {

    const iterateActive = async ()=>{
        const body= {id:props.elm._id}
        await setQuery('/api/loads/active/state', 'patch',body)
        props.changeCheck()

    }
    return (
            <div className = "warpper--posted_card" >
                <label htmlFor = "text" >_id
                    <input className = "text" type = "text" id = "_id" value={props.elm._id} readOnly= {true}/>
                </label >
                <label htmlFor = "created_by" >created_by
                    <input className = "text" type = "text" id = "created_by"
                           value={props.elm.created_by} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >delivery_address
                    <input className = "text" type = "text" id = "delivery_address" value={props.elm.delivery_address} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >name
                    <input className = "text" type = "text" id = "name" value={props.elm.name} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >payload
                    <input className = "text" type = "text" id = "payload" value={props.elm.payload} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >pickup_address
                    <input className = "text" type = "text" id = "pickup_address" value={props.elm.pickup_address} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >state
                    <input className = "text" type = "text" id = "state" value={props.elm.state} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >status
                    <input className = "text" type = "text" id = "status" value={props.elm.status} readOnly= {true}/>
                </label >

                <input className="login" type = "button" value="Change status" onClick={iterateActive}/>

            </div >
    );
};

export default CardAssignedLoads;
