import React, { useEffect, useState} from 'react';
import './Search-loads.scss'
import {setQuery} from "../../helper/helper";
import { useParams} from "react-router-dom";
const SearchLoads = (params) => {
    const {id} = useParams()
    const [loads, setLoads] =useState([])
    const [check, setCheck] =useState(true)
    const [message, setMessage] =useState('')

    const changeLoad =  async ()=>{
        try {
           const res =  await setQuery('/api/loads/'+id,'put',{...loads})
            if(res.status!==200) {
                throw  Error("Only status New");
            }
        } catch(e){
            setMessage(e.message)
            setTimeout(()=>{
                setMessage('')
            },1000)

        }
    }

    const fetchDate  = async () =>{
      return await setQuery('/api/loads/'+id)
    }

    useEffect(()=> {
         fetchDate().then(data=>{
             setLoads(({...data.data._doc}))

         })
        setCheck(false)
    },[check])

    return (
        <div >
                <div >{message}</div >
                <div className = "wrapper--search" >
                    <h1 >Change the load</h1 >
                    <label htmlFor = "text" >_id
                        <input className = "text" type = "text" id = "_id" value = {loads._id}
                               readOnly = {true} />
                    </label >
                    <label htmlFor = "email" >created_by
                        <input className = "text" type = "text" id = "created_by"
                               value = {loads.created_by} readOnly = {true} />
                    </label >


                    <label htmlFor = "text" >delivery_address
                        <input className = "text" type = "text" id = "delivery_address" value = {loads.delivery_address}
                               onChange = {(e) => setLoads({...loads, delivery_address: e.target.value})} />
                    </label >
                    <label htmlFor = "text" >name
                        <input className = "text" type = "text" id = "name" value = {loads.name}
                               onChange = {(e) => setLoads({...loads, name: e.target.value})} />
                    </label >
                    <label htmlFor = "text" >payload
                        <input className = "text" type = "number" id = "payload" value = {loads.payload}
                               onChange = {(e) =>
                                     setLoads({...loads, payload: e.target.value})
                               } />
                    </label >
                    <label htmlFor = "text" >pickup_address
                        <input className = "text" type = "text" id = "pickup_address" value = {loads.pickup_address}
                               onChange = {(e) => setLoads({...loads, pickup_address: e.target.value})} />
                    </label >
                    <label htmlFor = "text" >state
                        <input className = "text" type = "text" id = "state" value = {loads.state} readOnly = {true} />
                    </label >
                    <label htmlFor = "text" >status
                        <input className = "text" type = "text" id = "status" value = {loads.status}
                               readOnly = {true} />
                    </label >

                    {/*<label htmlFor = "text" >Width*/}
                    {/*    <input className = "text" type = "number" id = "width" value={loads.dimensions['width']}*/}
                    {/*            onChange={(e)=> {*/}
                    {/*                 const _dimensions = loads.dimensions*/}
                    {/*                 _dimensions.width=e.target.value*/}
                    {/*                 setLoads({...loads, dimensions:_dimensions })*/}
                    {/*             }}*/}
                    {/*    />*/}
                    {/* </label >*/}
                    {/*<label htmlFor = "text" >Length*/}
                    {/*    <input className = "text" type = "number" id = "text" value={loads.dimensions.length}*/}
                    {/*           onChange={(e)=> {*/}
                    {/*               const dimensions = loads.dimensions*/}
                    {/*               dimensions.length=e.target.value*/}
                    {/*               setLoads({...loads, dimensions:dimensions })*/}
                    {/*           }}/>*/}
                    {/*</label >*/}
                    {/*<label htmlFor = "text" >Height*/}
                    {/*    <input className = "text" type = "number" id = "text" value={loads.dimensions.height}*/}
                    {/*           onChange={(e)=> {*/}
                    {/*               const dimensions = loads.dimensions*/}
                    {/*               dimensions.height=e.target.value*/}
                    {/*               setLoads({...loads, dimensions:dimensions })*/}
                    {/*           }}/>*/}
                    {/*</label >*/}
                    <input className = "login" type = "button" value = "Change Load" onClick = {changeLoad} />

                </div >


        </div >
    )
};

export default SearchLoads;
