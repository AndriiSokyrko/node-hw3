import React from 'react';
import {Link} from "react-router-dom";
import './User-form-header.scss'
const UserFormHeader = () => {
    const logOut= ()=>{
        localStorage.setItem("jwt_token","")
    }
    return (
        <div className="user--form_header">
            <div className="logo">LOGO</div>
            <div className="wrapper--right">
                <div className="wrapper--right wrapper--right_name">{localStorage.getItem("email")}</div>
                <Link to="/" className="wrapper--right wrapper--right_logout" onClick={()=>logOut()}>LOGOUT</Link>

            </div>
        </div >
    );
};

export default UserFormHeader;
