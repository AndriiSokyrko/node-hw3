import React, { useState} from 'react';
import {setQuery} from "../../helper/helper";
import {useNavigate} from "react-router-dom";


const AddTruck = () => {
    const navigate = useNavigate()
    const [load,setLoad] = useState({name:'SPRINTER'
        })

const saveLoad= async ()=>{
    let body =  {type:load.name}
    await setQuery('/api/trucks','post',body)
    navigate('/user/truck')
}
const setName = (e) =>{
    const value = e.target.value
    setLoad({ name:value})
}

    return (
            <div className = "wrapper--trucks" >
                <label htmlFor = "name" >name
                    {/*<input className = "name" type = "text" id = "name"*/}
                    {/*       value = {load.name}*/}
                    {/*       onChange = {  setName} />*/}
                    <select onChange = {setName} >
                        <option name = "SPRINTER" id = "SPRINTER" value="SPRINTER">SPRINTER</option >
                        <option name = "SMALL STRAIGHT" id ="SMALL STRAIGHT"  vlaue="SMALL STRAIGHT" >SMALL STRAIGHT</option >
                        <option name = "LARGE STRAIGHT" id = "LARGE STRAIGHT" value="LARGE STRAIGHT">LARGE STRAIGHT</option >
                    </select >
                </label >
                <input className="save" type = "button" value="Save" onClick={saveLoad}/>
            </div >
    )

};
export default AddTruck;
