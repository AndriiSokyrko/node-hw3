import React from 'react';
import { Route, Routes} from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import Forgot from "./Forgot";
import UserForm from "./User-form";
import NewLoads from "./user-form/New-loads";
import PostedLoads from "./user-form/Posted-loads";
import AssignedLoads from "./user-form/Assigned-loads";
import Profile from "./user-form/Profile";
import History from "./user-form/History";
import AddLoad from "./add-load/Add-load";
import NewTrucks from "./user-form/New-trucks";
import AddTruck from "./add-truck/Add-truck";
import SearchLoads from "./search-load/Search-loads";

const AppRouters = () => {

    return (

            <Routes >
                    <Route path = "/login" element = {<Login />} exact = {Route.exact} key = {Route.path} />

                    <Route path = "/" element = {<Login />} exact = {Route.exact} key = {Route.path} />

                    <Route path = "/register" element = {<Register />} exact={Route.exact} key={Route.path} />

                    <Route path = "/login" element = {<Login />} exact={Route.exact} key={Route.path} />

                    <Route path = "/forgot" element = {<Forgot />} exact={Route.exact} key={Route.path} />

                    <Route path = "/user" element = {<UserForm />} exact={Route.exact} key={Route.path} >
                    <Route path = "new" element = {<NewLoads />} exact={Route.exact} key={Route.path} />
                    <Route path = "posted" element = {<PostedLoads />} exact={Route.exact} key={Route.path} />
                    <Route path = "assigned" element = {<AssignedLoads />} exact={Route.exact} key={Route.path} />
                    <Route path = "profile" element = {<Profile />} exact={Route.exact} key={Route.path} />
                    <Route path = "history" element = {<History />} exact={Route.exact} key={Route.path} />
                    <Route path = "addLoad" element = {<AddLoad />} exact={Route.exact} key={Route.path} />
                    <Route path = "addTruck" element = {<AddTruck />} exact={Route.exact} key={Route.path} />
                    <Route path = "truck" element = {<NewTrucks />} exact={Route.exact} key={Route.path} />
                    <Route path = "search/:id" element={<SearchLoads />}  exact={Route.exact} key={Route.path} />


                    </Route>

            </Routes >

    );
};

export default AppRouters;
