import React, {useCallback,  useState} from 'react';
import './Add-load.scss'
import {setQuery} from "../../helper/helper";
import {useNavigate} from "react-router-dom";

const AddLoad = () => {
    const navigate = useNavigate()
    // const [adress,setAddress]= useState('')
    // const [deliveryAddr,setDeliveryAddr]= useState('')
    const [load,setLoad] = useState({name:'',payload:0,pickup_address:'',
        delivery_address:'',dimensions:{
            width:'',
            length:'',
            height:''
        }})

const saveLoad= async ()=>{
    let body =  {...load}
    await setQuery('/api/loads','post',body)
    navigate('/user/new')
}
const setName = useCallback((e) =>{
    const value = e.target.value
    setLoad({...load, name:value})
},[load])

const setPayload = useCallback((e) =>{
    const value = e.target.value
    setLoad({...load, payload:value})
},[load])

const setPickup = useCallback((e) =>{
    const value = e.target.value
    setLoad({...load, pickup_address:value})
},[load])

const setDelivery = useCallback((e) =>{
    const value = e.target.value
    setLoad({...load, delivery_address:value})
},[load])

const setDimensionsW = useCallback((e) =>{
    const value = e.target.value
    const newDimesions = {...load.dimensions,width:value}
    setLoad({...load, dimensions:newDimesions })
},[load])

const setDimensionsL = useCallback((e) =>{
    const value = e.target.value
    const newDimesions = {...load.dimensions,length:value}
    setLoad({...load, dimensions:newDimesions })
},[load])

const setDimensionsH = useCallback((e) =>{
    const value = e.target.value
    const newDimesions = {...load.dimensions,height:value}
    setLoad({...load, dimensions:newDimesions })
},[load])




    return (
            <div className = "Auth" >


                <div className="wrapper--cards">
                Add new load
                <label htmlFor = "name" >name
                    <input className = "name" type = "text" id = "name" placeholder="name"
                           value = {load.name}
                           onChange = {  setName} />
                </label >

                <label htmlFor = "payload" >payload
                    <input className = "payload" type = "number" id = "payload" placeholder="payload"
                           value = {load.payload}
                           onChange = {  setPayload } />
                </label >
                <label htmlFor = "pickup_address" >pickup_address
                    <input className = "pickup_address" type = "text" id = "pickup_address" placeholder="pickup_address"
                           value = {load.pickup_address}
                           onChange = {  setPickup } />

                </label >
                <label htmlFor = "delivery_address" >delivery_address

                    <input className = "delivery_address" type = "text" id = "delivery_address" placeholder="delivery_address"
                           value = {load.delivery_address}
                           onChange = {  setDelivery } />
                </label >
                <label htmlFor = "width" >width
                    <input className = "width" type = "number" id = "width" placeholder="width"
                           value = {load.dimensions.width}
                           onChange = {  setDimensionsW } />
                </label >
                <label htmlFor = "length" >length
                    <input className = "length" type = "number" id = "length" placeholder="length"
                           value = {load.dimensions.length}
                           onChange = {  setDimensionsL } />
                </label >
                <label htmlFor = "height" >height
                    <input className = "height" type = "number" id = "height" placeholder="height"
                           value = {load.dimensions.height}
                           onChange = {  setDimensionsH } />
                </label >
                <input className="saveBtn" type = "button" value="Save" onClick={saveLoad}/>
                </div>
            </div >
    )

};
export default AddLoad;
