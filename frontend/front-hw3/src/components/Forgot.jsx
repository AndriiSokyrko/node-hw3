import React, { useState} from 'react';
import   './Forgot.scss'
import {useNavigate} from "react-router-dom";
import {setQuery} from "../helper/helper";
import * as emailjs from "@emailjs/browser";
const Forgot = () => {
    const navidate = useNavigate()
    const [email, setEmail] = useState('')
    const [status, setStatus] = useState('')
    const forgot = async ()=>{
        if(!email){
            return
        }
        const res = await setQuery('/api/auth/forgot_password', 'post', {email})
        if(res.status===200){
            setStatus('You\'re succesful changed')
             console.log(res);
            emailjs.send('service_v9uot5s','template_1zmoz16',
                {message: `Your new password: ${res.data.newPassword}`, reply_to: email,from_name: email},
                 'U8UAgUxru1dA7Fzpo')
                .then((result) => {
                    console.log(result.text);
                }, (error) => {
                    console.log(error.text);
                });
        } else{
            setStatus('You\'re not changed')
        }
        setTimeout(()=>navidate('/login'),1000)
    }
    return (
        <div className="auth">
            <div id="status_register">{status}</div>
            <div className="wrapper--title" >
                <a href = "/" className = "logo" >LOGO</a >
                <h1 className = "title" >Form forgot password</h1 ></div >
            <label htmlFor = "email" >
            <input className="email" type = "email" id="email" placeholder="Email" value={email}
                   onChange={e=>setEmail(e.target.value.trim())}/>
            </label >
            <input className="login" type = "button" value="Send" onClick={forgot}/>
        </div >
    );
};

export default Forgot;
