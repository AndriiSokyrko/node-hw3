import React, {useState} from 'react';
import './User-form-sidebar.scss'
import {NavLink, useNavigate} from "react-router-dom";

const UserFormSidebar = () => {
    const navigate = useNavigate()
    const [searchId, setSearchId] = useState('')

    const changeSearch =(e)=>{
        setSearchId(e.target.value)
        }

    const searchById =async (e) =>{
        e.preventDefault()
        navigate("search/"+searchId,{forceRedir: true})
    }

    return (
        <div className="wrapper--side_bar">
            <nav className="wrapper--side wrapper--side_bar_nav ">
                <form className = "d-flex" >
                         <input id = "searchValueId" className = "form-control me-2" type = "search" value={searchId}
                           placeholder = "Search by Id" aria-label = "Search" onChange={changeSearch}/>
                           <button id = "searchById" className = "btn btn-outline-success" type = "submit"
                                   onClick={searchById}>
                           &#128269;
                           </button>

                </form >

                    {localStorage.getItem("role")==='DRIVER' ?
                        <ul>
                        <li ><NavLink to = "new" >NEW LOADS</NavLink ></li >
                        <li ><NavLink to = "truck" >NEW TUCK</NavLink ></li >
                        <li ><NavLink to = "posted" >POSTED LOADS</NavLink ></li >
                        <li ><NavLink to = "assigned" >ASSIGNED LOADS</NavLink ></li >
                        <li ><NavLink to = "history" >HISTORY</NavLink ></li >
                        <li ><NavLink to = "profile" >PROFILE</NavLink ></li >
                        </ul>
                        :
                        <ul >
                            <li ><NavLink to = "new" >NEW LOADS</NavLink ></li >
                            <li ><NavLink to = "history" >HISTORY</NavLink ></li >
                            <li ><NavLink to = "profile" >PROFILE</NavLink ></li >
                        </ul >
                    }
            </nav>
        </div >
    );
};

export default UserFormSidebar;
