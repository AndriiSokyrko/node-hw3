import React from 'react';
import './Card-trucks.scss'
import {setQuery} from "../../helper/helper";
import {useNavigate} from "react-router-dom";
const CardTrucks = (props) => {

    const navigate = useNavigate()
    const assignProfile = async ()=>{
        const id= props.elm.elm._id
        await setQuery(`/api/trucks/${id}/assign`, 'post',{_id:id})
        props.setCheck()
        navigate("/user/truck", { replace: true });

    }
const deleteProfile = async ()=>{
    const id= props.elm.elm._id
    await setQuery('/api/trucks/'+id, 'delete')
    props.setCheck()
    navigate("/user/truck", { replace: true });

}
    return (
            <div className = "wrapper--tricks_card" >
                <label htmlFor = "text" >_id
                    <input className = "text" type = "text" id = "text"
                           value={props.elm.elm._id} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >created_by
                    <input className = "text" type = "text" id = "created_by"
                           value={props.elm.elm.created_by} readOnly= {true}/>
                </label >

                <label htmlFor = "status" >status
                    <input className = "text" type = "text" id = "status"
                           value={props.elm.elm.status} readOnly= {true}/>
                </label >
                <label htmlFor = "text" >Type
                    <input className = "text" type = "text" id = "type"
                           value={props.elm.elm.type} readOnly= {true}/>
                </label>
                <label htmlFor = "text" >
                    Assigned
                    <input className = "text" type = "text" id = "assigned_to"
                           value={props.elm.elm.assigned_to} readOnly= {true}  />
                </label>

                <div className="wrapper--btn">
                    <input className="assign" type = "button" value="Assign" onClick={assignProfile}/>
                    <input className="delete" type = "button" value="Delete" onClick={deleteProfile}/>

                </div>

            </div >
    );
};

export default CardTrucks;
